const db = require("../models")
const Article = db.article;

exports.create = (req, res) => {
    if (!req.body.title) {
        req.status(400).send(
            {
                message: "Content can not be empty!"
            }
        );
        return;
    }
    const article = new Article({
        title: req.body.title,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
    });

    article.save(article)
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error ocuured while creating article"
            });
        });

};

exports.findAll = (req, res) => {
    const title = req.query.title;
    let condition = title ? {
        title: {
            $regex: new RegExp(title),
            $options: "i"
        }
    } : {};
    Article.find(condition)
        .then(data => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving articles"
            })
        })
}

exports.findOne = (req, res) => {
    const id = req.params.id;

    Article.findById(id)
        .then((data) => {
            if (!data) {
                res.status(404).send({ message: "Not found Article with id " + id });
            }
            else {
                res.send(data);
            }
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Article with id=" + id });
        });
};

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Article.findByIdAndUpdate(id, req.body,
        {
            useFindAndModify: false
        })
        .then(data => {
            if (!data) {
                res.status(404).send(
                    {
                        message: `Cannot update Article with id=${id}. Maybe article was not found!`
                    }
                );
            }
            else {
                res.send(
                    {
                        message: "Article was updated successfully."
                    }
                )
            };
        })
        .catch((err) => {
            res.status(500).send(
                {
                    message: "Error updating article with id=" + id
                }
            );
        });

};

exports.delete = (req, res) => {
    const id = req.params.id;
    Article.findByIdAndRemove(id)
        .then((data) => {
            if (!data) {
                res.status(404).send(
                    {
                        message: `Cannot delete article with id : ${id}. May be article was not found`
                    }
                );
            }
            else {
                res.send(
                    {
                        message: "Article deleted successfully"
                    }
                )
            }
        })
        .catch((err) => {
            res.status(500).send(
                {
                    message: `Could not delete article with id : ${id}`
                }
            );
        })
};

exports.deleteAll = (req, res) => {
    Article.deleteMany({})
    .then((data)=>{
        res.send(
            {
                message: `${data.deletedCount} articles were deleted succesfully`
            }
        )
    })
    .catch((err)=>{
        res.status(500).send(
            {
                message: err.message || "Some error occured while deleting all articles"
            }
        )
    })
};

exports.findAllPublished = (req, res) => {
    Article.find(
        {
            published:true
        }
    )
    .then((data)=>{
        res.send(data)
    })
    .catch((err)=>{
        res.status(500).send(
            {
                message : err.message || "Some error occurred while retrieving articles"
            }
        )
    })
};