const dbconfig = require("../config/config.js");
const articleModel = require("./article.model.js");
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbconfig.url;
db.article = articleModel(mongoose);

module.exports = db;