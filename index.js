const cors = require('cors');
const db = require('./app/models')
const articles = require('./app/routes/articles.routes')
const express = require('express');

const PORT = process.env.PORT || 8080;
const app = express();

const corsOptions = {
    origin: "http://localhost:8080"
}

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.json({
        message: "Welcome to dummy api",
        GET_apiForAllArticles : "api/articles",
        GET_apiForArticle: "api/articles/:id",
        POST_apiForCreatingArticles : "api/articles",
        PUT_apiForUpdatingArticles : "api/articles/:id",
        DELETE_apiForDeleteAllArticles: "api/articles",
        DELETE_apiForDeleteArticle : "api/articles/:id",
    })
})

db.mongoose
.connect(db.url,{
    useNewUrlParser : true,
    useUnifiedTopology: true
})
.then(()=>{
    console.log("connected to db");
})
.catch((err)=>{
    console.log(`${err} to connect db`);
    process.exit();
})

articles(app);

app.listen(PORT, () => {
    console.log(`listening at ${PORT}...`)
});